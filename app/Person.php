<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;


class Person extends Model
{
    use SingleTableInheritanceTrait;

    protected $table = "people";

    protected static $singleTableTypeField = 'type';

    protected static $singleTableSubclasses = [Manager::class,Student::class];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','nickname','birthday','cpf','rg','email','phone','cellphone','address_id','program_id','year','center_id','photo_id','user_id'];

    /**
     * The attributes appended from the model's JSON form.
     *
     * @var array
     */
    protected $appends = ['victories'];


    public function address(){
        return $this->belongsTo(Address::class,'address_id');
    }

    public function curriculum(){
        return $this->hasOne(Curriculum::class,'person_id');
    }

    public function photo(){
        return $this->belongsTo(Photo::class,'photo_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function preferenceCities(){
        return $this->belongsToMany(City::class,'opportunity_profile_cities','person_id', 'city_id')->with('state');
    }
    public function archivedOpportunities(){
        return $this->belongsToMany(Opportunity::class,'opportunity_people','person_id', 'opportunity_id')->with('company');
    }

    public function wonGames(){
        return $this->hasMany(CompetitiveGame::class,'winner_id');
    }

    public function getVictoriesAttribute(){
        return count($this->wonGames);
    }

    
}
