<?php

namespace App\Helpers;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model as PaginatorModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class QueryStringHelper
{
    /**
     * @var string
     */
    private $model;

    /**
     * @var mixed
     */
    private $modelInstance;

    private $query;

    private $entities;

    /**
     * @var array
     */
    private $queryString;

    /**
     * @var array
     */
    private $currentUrl;

    /**
     * @var string
     */
    public $conditions = "";

    /**
     * @var string
     */
    public $order = "";

    /**
     * @var string
     */
    private $columns = "";

    /**
     * @var array
     */
    private $bind = [];

    /**
     * @var PaginatorModel
     */
    private $paginator = null;

    /**
     * @var array
     */
    private $reservedParams = ['sort', 'fields', 'page','per_page', 'count','random','limit'];

    private $fields = [];

    private $optinalParams = [];

    private $with = [];

    /**
     * @param Request $request
     * @param mixed $model
     * @param array $optionalParams
     * @param array $with
     */
    public function __construct($request, $model,$optionalParams = [],$with = [],$orderBy = 'id')
    {

        parse_str($request->getQueryString(),$this->queryString);
        $this->currentUrl = $request->getUri();
        $this->model = $model;
        $this->modelInstance = new $this->model;
        //$this->entities = call_user_func_array([$this->model,'where'],['active',1]);
        $this->query = $model::with($with);
        $this->optinalParams = $optionalParams;
        $this->fields = DB::connection($this->modelInstance->getConnectionName())->getSchemaBuilder()->getColumnListing($this->modelInstance->getTable());
        $this->with = $with;

    }

    /**
     * Perform all tools and fill model params
     *
     * @return void
     */
    public function run()
    {
        $this->runFilters();
        $this->runFields();
        $this->runSort();

    }

    public function mountModelParams(){
        $modelParams = [];

        if($this->conditions !== ''){
            $modelParams['conditions'] = $this->conditions;
        }
        if(count($this->bind) > 0){
            $modelParams['bind'] = $this->bind;
        }
        if($this->order !== ''){
            $modelParams['order'] = $this->order;
        }
        if($this->columns !== ''){
            $modelParams['columns'] = $this->columns;
        }

        return $modelParams;
    }

    /**
     * Perform all tools and return the model params
     *
     * @return array
     */
    public function generateModelParams()
    {
        $this->run();
        return $this->mountModelParams();
    }

    /**
     * Perform filters
     *
     * @return void
     */
    public function runFilters(){
        if(count($this->queryString) == 0){
            return;
        }

        foreach ($this->queryString as $key => $value) {
            $operator = "eq";
            if(strpos($key, '$') !== false){
                $operator = explode('$', $key)[1];
                $key = explode('$', $key)[0];
            }

            if(!in_array($key,$this->fields) || in_array($key, $this->reservedParams)) {
                continue;
            }

            switch($operator){
                case 'lk':
                    $this->query->where($key,'like','%'.$value.'%');
                    break;
                case 'gt':
                    $this->query->where($key,'>',$value);
                    break;
                case 'gte':
                    $this->query->where($key,'>=',$value);
                    break;
                case 'lt':
                    $this->query->where($key,'<',$value);
                    break;
                case 'lte':
                    $this->query->where($key,'<=',$value);
                    break;
                case 'bw':
                    $value = explode(',',$value); // TODO: Verificar array pra o metodo $_GET
                    if(is_array($value) && count($value) == 2) {
                        $this->query->whereBetween($key,[$value[0],$value[1]]);
                    }
                    break;
                case 'in':
                    $value = explode(',',$value); // TODO: Verificar array pra o metodo $_GET
                    if(is_array($value) && count($value) == 2) {
                        $this->query->whereIn($key,$value);
                    }
                    break;
                case 'eq':
                default:
                    $this->query->where($key,$value);
                    break;
            }
        }

    }

    /**
     * Perform sort
     *
     * @return void
     */
    public function runSort()
    {

        if(!isset($this->queryString['order']) || empty($this->queryString['order'])){
            return;
        }


        $fieldsToOrderArr = explode(',', $this->queryString['order']);
        $orderArr = [];

        foreach($fieldsToOrderArr as $f){
            $type = "ASC";
            if(substr($f, 0, 1) === "+" || substr($f, 0, 1) === " "){
                $type = "ASC";
                $field = substr($f, 1);
            }elseif(substr($f, 0, 1) === "-"){
                $type = "DESC";
                $field = substr($f, 1);
            }else{
                $field = $f;
            }

            if(!in_array($field,$this->modelInstance->getFillable())) {
                continue;
            }

            $this->query->orderBy($field,$type);
            $orderArr[] = "{$field} $type";
        }

        if(!empty($this->order)){
            //$orderArr[] = $this->order;
            //array_unshift($orderArr, $this->order);
        }

        $this->order = implode(",", $orderArr);
    }

    /**
     * Perform fields
     *
     * @return void
     */
    public function runFields()
    {

        if(!isset($this->queryString['fields']) || empty($this->queryString['fields'])){
            return;
        }

        $fieldsArr = explode(',', $this->queryString['fields']);
        $columnsArr = "";

        foreach($fieldsArr as $field){
            if(!in_array($field,$this->fields)) {
                continue;
            }

            $columnsArr[] = $field;

        }

        if(!empty($this->columns)){
            $columnsArr[] = $this->columns;
        }

        $this->query->select($columnsArr);


        $this->columns = implode(",", $columnsArr);
    }

    /**
     * @return array
     */
    public function getQueryString()
    {
        return $this->queryString;
    }

    /**
     * @param array $queryString
     */
    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;
    }

    public function getResult(){
        $this->runFields();
        $this->runFilters();
        $this->runSort();

        //die($this->query->toSql());


        //$this->query->with($this->with);

        if(isset($this->queryString['page']) | isset($this->queryString['per_page'])){

            $per_page = null;
            if(isset($this->queryString['per_page'])){
                $per_page = $this->queryString['per_page'];
            }

            $params = $this->currentUrl;
            $params = preg_replace('([&]?per_page=\d+)',"",$params);
            $params = preg_replace('([&]?page=\d+)',"",$params);

            $result = $this->query->paginate($per_page?:15);
            $result->setPath($params);
            $result->addQuery('per_page', $per_page);

            return $result;
        }else{
            return $this->query->get();
        }

    }

    public function getQuery($pagination = false){
        $this->runFields();
        $this->runFilters();
        if($pagination == true){
            $this->runSort();
        }

        return $this->query;
    }



}
