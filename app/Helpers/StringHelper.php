<?php
/**
 * Created by PhpStorm.
 * User: dola
 * Date: 06/06/2016
 * Time: 11:28
 */

/**
 * Merges two strings in a way that a pattern like ABABAB will be
 * the result.
 *
 * @param     string $str1 String A
 * @param     string $str2 String B
 * @return    string    Merged string
 */

namespace App\Helpers;

class StringHelper
{

    static function MergeBetween($str1, $str2)
    {
        // Split both strings
        $str1 = str_split($str1, 1);
        $str2 = str_split($str2, 1);

        // Swap variables if string 1 is larger than string 2
        if (count($str1) >= count($str2))
            list($str1, $str2) = array($str2, $str1);

        // Append the shorter string to the longer string
        for ($x = 0; $x < count($str1); $x++)
            $str2[$x] .= $str1[$x];

        return implode('', $str2);
    }

    static function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

}