<?php
/**
 * Created by PhpStorm.
 * User: dola
 * Date: 16/08/2016
 * Time: 11:47
 */

namespace App;

use Illuminate\Support\Facades\Auth;


class FacebookGrantVerifier
{
    public function verify($username, $password)
    {
        $user = User::where('email',$username)->where('facebook_id',$password)->first();

        if(!is_null($user)){
            return $user->id;
        }

        return false;
    }
}