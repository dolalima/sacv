<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Person
{
    protected static $singleTableType = 'manager';
}
