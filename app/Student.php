<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Person
{
    protected static $singleTableType = 'student';

    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['address_id','photo_id','user_id'];

    
    
}
