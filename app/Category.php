<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Entity
{
    protected $table = 'categories';

    protected $fillable = ['name','slug', 'parent_id'];

    public function parent(){
      return $this->hasOne(Category::class,'parent_id');
    }

    public function childrens(){
      return $this->hasMany(Category::class,'categories','parent_id');
    }
}
