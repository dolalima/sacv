<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{

    public function roles(){
        return $this->belongsToMany(Role::class,'permission_role','role_id','permission_id');
    }
    
}
