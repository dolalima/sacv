<?php
/**
 * Created by PhpStorm.
 * User: dola
 * Date: 21/11/2016
 * Time: 15:38
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Entity extends Model
{

    protected $rules_validation = [];


    public function validate($input){
        return Validator::make($input,$this->rules_validation);
    }

}