<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    function activities(){
        return $this->hasMany(EventActivity::class,'event_id');
    }
}
