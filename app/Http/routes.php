<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::post('oauth/access_token',function(){
    return Response::json(Authorizer::issueAccessToken());
});
Route::post('oauth/facebook',function(){
    return Response::json(Authorizer::issueAccessToken());
});



Route::get('/', function () {
    $events = \App\Event::all()->get(25);
    $categories = \App\Category::get();
    return view('site.home',compact('events','categories'));
});



Route::resource('events','SiteEventController');
Route::get('events/category/{slug}','SiteEventController@index')->name('events.category');

Route::auth();
Route::get('/home', function(){return view('layouts.site');});

Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
    Route::get('/',function(){return view('home');});
    Route::resource('managers','ManagerController');
    Route::resource('events','EventController');
    Route::resource('categories','CategoryController');
    Route::resource('roles','RoleController');
    Route::resource('permissions','PermissionController');
});
