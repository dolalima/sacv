<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      $data = Category::orderBy('id','DESC')->paginate(5);
      return view('categories.index',compact('data'))
          ->with('i', ($request->input('page', 1) - 1) * 5);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      $categories = Category::lists('name','id');
      return view('categories.create',compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $this->validate($request, [
          'name' => 'required',
          'slug' => 'required',
      ]);
      try {
          DB::beginTransaction();

          $input = $request->all();
          $category = Category::create($input);
          $category->save();
          DB::commit();
      }catch (Exception $e){
          DB::rollback();
      }

      return redirect()->route('admin.categories.index')
          ->with('success','Category created successfully');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $category = Category::with('parent')->find($id);
      return view('categories.show',compact('category'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $category = Category::find($id);
      $categories = Category::lists('name','id');


      return view('categories.edit',compact('category','categories'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      $this->validate($request, [
          'name' => 'required',
          'slug' => 'required',
      ]);

      $input = $request->all();


      $category = Category::find($id);
      $category->update($input);
      $category->save();

      return redirect()->route('categories.index')
          ->with('success','Category updated successfully');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      Category::find($id)->delete();
      return redirect()->route('categories.index')
          ->with('success','Category deleted successfully');
  }
}
