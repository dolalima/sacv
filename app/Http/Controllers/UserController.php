<?php

namespace App\Http\Controllers;

use App\Address;
use App\Person;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $result = ['success' => false, 'message' => '','errors'=>[]];

        $validator = Validator::make($request->input(),[
            'person.cpf' =>'required',
            'person.year' => 'required',
            'person.program_id' =>'required',
            'person.center_id' => 'required',
            'person.name' =>'required',
            'person.photo'=>'required',
            'person.nickname'=>'required',
            'person.email' =>'required'
        ]);

        DB::beginTransaction();

        try {

            if ($validator->fails()) {
                $result['errors'] = $validator->errors()->all();
                throw new \Exception("Dados Invalidos");
            }

            $data = $request->input();

            $user = User::with('person.address')->find($id);

            $person = Person::with('address')->where('user_id',$id)->first();
            $person->fill($data['person']);

            if(empty($person->address)){
                $address = Address::create($data['person']['address']);
                $address->save();
                $person->address_id = $address->id;
            }else{
                $person->address->update($data['person']['address']);
            }

            $user->save();
            $person->save();

            DB::commit();

            $result['success'] = true;
            $result['message'] = "Dados cadastrados com sucesso";

        }catch(\Exception $e){
            $result['message'] = $e->getMessage();
            DB::rollback();
        }

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
