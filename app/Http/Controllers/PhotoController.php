<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Photo;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = ['success' => false, 'message' => '','errors'=>[]];

        $validatorPhoto = Validator::make($request->input(),[
            'image'=>'required',
            'path' =>'required'
        ]);

        try {

            if ($validatorPhoto->fails()) {
                $result['errors'] = $validatorPhoto->errors()->all();
                throw new \Exception("Dados Invalidos");
            }

            DB::beginTransaction();

            $data = $request->input();

            $photo = Photo::create();
            $time = time();
            $photo->path = $data['path'];
            $photo->file = sha1(strval($time))+'-'+$time.".png";

            $image_data = $data['image'];
            $image_data = str_replace('data:image/png;base64,', '', $image_data);
            $image_data = str_replace(' ', '+', $image_data);

            $image_data = base64_decode($image_data);

            file_put_contents(public_path().$photo->path.$photo->file, $image_data);

            $photo->save();

            DB::commit();

            $result['success'] = true;
            $result['message'] = "Foto cadastrados com sucesso";
            $result['data'] = $photo;

        }catch(\Exception $e){
            DB::rollBack();
            $result['message'] = $e->getMessage();
        }

        return $result;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
