<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\State;

class StateController extends BaseResourceController
{

    /**
     * StateController constructor.
     */
    public function __construct()
    {
        $this->model = new State();
    }
}
