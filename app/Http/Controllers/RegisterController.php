<?php

namespace App\Http\Controllers;

use App\Role;
use App\Student;
use Illuminate\Http\Request;
use App\User;
use App\Person;
use App\Address;
use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
    public function create()
    {
        $request = Request::capture();
        $result = ['success' => false, 'message' => '','errors'=>[]];

        $validator = Validator::make($request->input(),[
            'cpf' =>'required|unique',
            'year' => 'required',
            'program_id' =>'required',
            'center_id' => 'required',
            'name' =>'required',
            'email' =>'required|unique',
            'password' => 'required|same:passwordCheck',
        ]);




        try {

//            if ($validator->fails()) {
//
//                $result['errors'] = $validator->errors()->all();
//                throw new \Exception("Dados Invalidos");
//            }

            if(Person::where('cpf',$request->input('cpf'))->count()>0){

                throw new \Exception("CPF já cadastrado no sistema");
            }

            if(User::where('email',$request->input('email'))->count()>0){

                throw new \Exception("Email já cadastrado no sistema");
            }



            $data = $request->input();

            $data['password'] = bcrypt($data['password']);

            $person = Student::create($data);
            
            $address = Address::create([]);
            $address->save();

            $user = User::create($data);


            $user->save();

            $person->address_id = $address->id;
            $person->user_id = $user->id;

            $person->save();

            $result['success'] = true;
            $result['message'] = "Dados cadastrados com sucesso";
        }catch(\Exception $e){
            $result['exception'] = $e->getMessage();
        }

        return $result;

    }


    public function facebook(){

        $request = Request::capture();
        $result = ['success' => false, 'message' => '','errors'=>[]];

        $validator = Validator::make($request->input(),[
            'name' =>'required',
            'email' =>'required|unique:users',
            'id' => 'required',
        ]);


        try {
            DB::beginTransaction();

            if ($validator->fails()) {
                $result['errors'] = $validator->errors()->all();
                throw new \Exception("Dados Invalidos");
            }

            if(User::where('email',$request->input('email'))->count()>0){
                throw new \Exception("Email já cadastrado no sistema");
            }
            $data = $request->input();

            $data['facebook_id'] = $data['id'];

            $student = Student::create($data);
            $user = User::create($data);

            $user->save();

            $student->user_id = $user->id;

            $student->save();

            DB::commit();

            $result['success'] = true;
            $result['message'] = "Dados cadastrados com sucesso";
        }catch(\Exception $e){
            $result['message'] = $e->getMessage();
            DB::rollback();
        }

        return $result;

    }

}
