<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Role;
use App\Manager;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Manager::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::lists('display_name','id');
        return view('users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email||unique:people,email',
            'password' => 'required|same:confirm-password',
            'user.roles' => 'required'
        ]);
        try {
            DB::beginTransaction();

            $input = $request->all();
            $input['password'] = Hash::make($input['password']);

            $manager = Manager::create($input);
            $user = User::create($input);
            foreach ($request->input('user.roles') as $key => $value) {
                $user->roles()->attach($value);
            }
            $user->save();
            $manager->user_id = $user->id;

            $manager->save();
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
        }

        return redirect()->route('admin.managers.index')
            ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manager = Manager::find($id);
        $roles = Role::lists('display_name','id');
        $userRole = $manager->user->roles->lists('id','id')->toArray();

        return view('users.edit',compact('manager','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:people,email,'.$id,
            'password' => 'same:confirm-password',
            'user.roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));
        }

        $manager = Manager::find($id);
        $manager->update($input);

        $manager->user->update($input);

        DB::table('role_user')->where('user_id',$manager->user->id)->delete();


        foreach ($request->input('user.roles') as $key => $value) {
            $manager->user->roles()->attach($value);
        }

        return redirect()->route('admin.managers.index')
            ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Manager::find($id)->delete();
        return redirect()->route('admin.managers.index')
            ->with('success','User deleted successfully');
    }
}
