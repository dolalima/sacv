<?php
/**
 * Created by PhpStorm.
 * User: dola
 * Date: 21/11/2016
 * Time: 14:47
 */

namespace App\Http\Controllers;


use App\Entity;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Helpers\QueryStringHelper;

class BaseResourceController extends AuthController
{
    /**
     * @var Entity $model
     */
    protected $model;

    /**
     * @var  array $optionalParams
     */
    protected $optionalParams =[];

    /**
     * @var array $withs
     */
    protected $withs = [];

    /**
     * @var string $route
     */
    protected $route = '';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $queryStringHelper = new QueryStringHelper(Request::capture(),$this->model,$this->optionalParams,$this->withs);
        return $queryStringHelper->getResult();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = ['success' => false, 'message' => '','errors'=>[]];

        $validation = $this->model->validate($request->input());

        try {
            if ($validation->fails()) {
                $result['errors'] = $validation->errors()->all();
                throw new \Exception("Dados Invalidos");
            }

            DB::beginTransaction();

            $data = $request->input();
            $entity = $this->model->create($data);
            $entity->save();

            DB::commit();

            $result['success'] = true;
            $result['message'] = "Dados cadastrados com sucesso";

        }catch(\Exception $e){
            DB::rollBack();
            $result['message'] = $e->getMessage();
            $request['exception'] = $e->getTraceAsString();
        }

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->model->with($this->withs)->find($id);
        return $model;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = ['success' => false, 'message' => '','errors'=>[]];

        $validation = $this->model->validate($request->input());

        try {
            if ($validation->fails()) {
                $result['errors'] = $validation->errors()->all();
                throw new \Exception("Dados Invalidos");
            }

            DB::beginTransaction();

            $data = $request->input();
            $entity = $this->model->find($id);
            $entity->fill($data);
            $entity->save();

            DB::commit();

            $result['success'] = true;
            $result['message'] = "Dados atualizados com sucesso";

        }catch(\Exception $e){
            DB::rollBack();
            $result['message'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = ['success' => false, 'message' => '','errors'=>[]];


        try {

            DB::beginTransaction();
            $entity = $this->model->find($id);

            if(is_null($entity)){
                throw new \Exception('Entidade não encontrada');
            }

            $this->model->destroy($id);

            DB::commit();

            $result['success'] = true;
            $result['message'] = "Dados excluidos com sucesso";

        }catch(\Exception $e){
            DB::rollBack();
            $result['message'] = $e->getMessage();
        }

        return $result;
    }
}