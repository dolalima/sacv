<?php

namespace App\Http\Controllers;

use App\City;
use App\Person;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\User;
use App\Curriculum;
use App\Http\Requests;

class AuthController extends Controller
{

    protected $user = null;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {

    }

    public function getUser(){
        $user = User::with('person.photo','person.address.city','person.curriculum')->find(Authorizer::getResourceOwnerId());
        return $user;
    }

    /**
     * @return Person
     */
    public function getPerson(){
        $person = Person::with(['photo','address.city'])->where('user_id',Authorizer::getResourceOwnerId())->first();
        return $person;
    }

    public function getCurriculum(){
        $curriculum = Curriculum::with(['photo','person','address'])->where('person_id',$this->getPerson()->id)->first();
        return $curriculum;
    }

    public function getOpportunityPreferenceCities(){
        $cities = $this->getPerson()->opportunityPreferenceCities();
        return $cities;
    }

}
