<?php
/**
 * Created by IntelliJ IDEA.
 * User: dolal
 * Date: 15/05/2017
 * Time: 22:30
 */

// Model

// 'enity-staus-action'

return [
    // Action
    'edit'=>'edit',
    'delete'=>'delete',
    'show'=>'show',
    // User
    'user'=>'user',
    'user-new'=>'Create New User',
    'user-list'=>'user list',
    'user-save-sucess'=>'user saved successfully',
    'user-save-fail'=>'user unsaved',

    // Roles
    'role'=>'regra' ,
    'roles'=>'regras',
    'role-new'=>'Nova Regra',
    'role-list'=>'Lista de Regras',
    'role-save-success'=>'Regra salva com sucesso.',
    'role-save-fail'=>'Erro ao salva a regra.',

    // Address
    'street'=>'',
];