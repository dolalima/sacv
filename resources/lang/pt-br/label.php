<?php
/**
 * Created by IntelliJ IDEA.
 * User: dolal
 * Date: 15/05/2017
 * Time: 22:30
 */

// Model

// 'enity-staus-action'

return [

    // Fileds
    'name-field'=>'Nome',
    'description-field'=>'Descrição',
    'initial-date-field'=>'Data de Inicio',
    'final-date-field'=>'Data do Fim',

    // Common Action
    'create'=>'Criar',
    'edit'=>'Editar',
    'delete'=>'excluir',
    'show'=>'Mostrar',
    'save'=>'Salvar',
    'back'=>'Voltar',
    'cancel'=>'Cancelar',
    'save-success'=>'Salvo com sucesso.',
    'save-fail'=>'Atenção! Erro ao salvar.',

    // User
    'user'=>'Usuário',
    'users'=>'Usuários',
    'user-new'=>'Novo Usuário',
    'user-list'=>'Lista de Usuários',
    'user-save-sucess'=>'Usuário salvo com exito.',
    'user-save-fail'=>'Erro ao salva o usuário.',

    // Roles
    'role'=>'regra' ,
    'roles'=>'regras',
    'role-new'=>'Nova Regra',
    'role-list'=>'Lista de Regras',
    'role-save-success'=>'Regra salva com sucesso.',
    'role-save-fail'=>'Erro ao salva a regra.',

    // Events
    'event'=>'Evento',
    'events'=>'Eventos',
    'event-new'=>'Novo Evento',
    'event-list'=>'Lista de Eventos',
    'event-save-sucess'=>'Evento salvo com exito.',
    'event-save-fail'=>'Erro ao salva o evento.',

    // Categories
    'category'=>'Categoria',
    'categories'=>'Categorias',
    'category-new'=>'Nova Categoria',
    'category-list'=>'Lista de Categorias',
    'category-save-success'=>'Categoria salva com exito.',
    'category-save-fail'=>'Erro ao salva a categoria.',

    // Address
    'street'=>'',
];
