@extends('layouts.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@lang('label.user-list')</h2>
                        </div>
                        <div class="pull-right">
                            @permission('manager-create')
                            <a class="btn btn-success" href="{{ route('admin.managers.create') }}"><i class="fa fa-plus"></i> @Lang('label.user-new')</a>
                            @endpermission
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @Lang('label.users')
                    </div>
                    <div class="panel-body" style="padding: 0px">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Roles</th>
                                <th width="280px">Action</th>
                            </tr>
                            @foreach ($data as $key => $manager)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $manager->name }}</td>
                                    <td>{{ $manager->email }}</td>
                                    <td>
                                        @if(!empty($manager->user->roles))
                                            @foreach($manager->user->roles as $v)
                                                <label class="label label-success">{{ $v->display_name }}</label>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.managers.show',$manager->id) }}">Show</a>
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.managers.edit',$manager->id) }}">Edit</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $manager->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                {!! $data->render() !!}
            </div>
        </div>
    </div>
@endsection