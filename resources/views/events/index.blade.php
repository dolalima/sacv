@extends('layouts.dashboard')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>@lang('label.event-list')</h2>
                    </div>
                    <div class="pull-right">
                        @permission('manager-create')
                        <a class="btn btn-success" href="{{ route('admin.events.create') }}"><i class="fa fa-plus"></i> @Lang('label.event-new')</a>
                        @endpermission
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    @Lang('label.events')
                </div>
                <div class="panel-body" style="padding: 0px">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($data as $key => $event)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $event->name }}</td>
                            <td>
                                <a class="btn btn-xs btn-info" href="{{ route('admin.events.show',$event->id) }}">Show</a>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.events.edit',$event->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $event->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            {!! $data->render() !!}
        </div>
    </div>
</div>
@endsection