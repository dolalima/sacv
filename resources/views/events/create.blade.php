@extends('layouts.dashboard')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>@Lang('label.event-new')</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('admin.managers.index') }}"> Back</a>
                    </div>
                </div>
            </div>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    @Lang('label.event')
                </div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'admin.managers.store','method'=>'POST')) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@Lang('label.name-field'):</strong>
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col col-xs-12">
                            <div class="form-group">
                                <strong>Banner</strong>
                                {!! Form::file('image') !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>@Lang('label.description-field'):</strong>
                                {!! Form::text('description', null, array('placeholder' => 'Breve descrição do evento','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <strong>@Lang('label.initial-date-field'):</strong>
                            {!! Form::date('initial_date',null,array('placeholder'=>'dd/mm/aaaa','class'=>'form-control datepicker'))!!}
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <strong>@Lang('label.final-date-field'):</strong>
                            {!! Form::date('final_date',null,array('placeholder'=>'dd/mm/aaaa','class'=>'form-control datepicker'))!!}
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Conteudo</strong>
                                {!! Form::textArea('content', null, array('placeholder' => 'Email','class' => 'form-control text-editor')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
