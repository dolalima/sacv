@extends('layouts.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@Lang('label.role-list')</h2>
                        </div>
                        <div class="pull-right">
                            @permission('role-create')
                            <a class="btn btn-success" href="{{ route('admin.roles.create') }}"><i class="fa fa-plus"></i> @Lang('label.role-new')</a>
                            @endpermission
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @Lang('label.roles')
                    </div>
                    <div class="panel-body" style="padding: 0px">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th width="280px">Action</th>
                            </tr>
                            @foreach ($roles as $key => $role)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>

                                        <a class="btn btn-xs btn-info" href="{{ route('admin.roles.show',$role->id) }}">Show</a>
                                        @permission('role-edit')
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.roles.edit',$role->id) }}">Edit</a>
                                        @endpermission
                                        @permission('role-delete')
                                        {!! Form::open(['method' => 'DELETE','route' => ['admin.roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                        @endpermission

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                {!! $roles->render() !!}
            </div>
        </div>
    </div>
@endsection