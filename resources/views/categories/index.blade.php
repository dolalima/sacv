@extends('layouts.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>@Lang('label.category-list')</h2>
                        </div>
                        <div class="pull-right">
                            @permission('category-create')
                            <a class="btn btn-success" href="{{ route('admin.categories.create') }}"><i class="fa fa-plus"></i> @Lang('label.category-new')</a>
                            @endpermission
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @Lang('label.categories')
                    </div>
                    <div class="panel-body" style="padding: 0px">
                        <table class="table table-bordered">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th width="280px">Action</th>
                            </tr>
                            @foreach ($data as $key => $category)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td>

                                        <a class="btn btn-xs btn-info" href="{{ route('admin.categories.show',$category->id) }}">Show</a>
                                        @permission('category-edit')
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.categories.edit',$category->id) }}">Edit</a>
                                        @endpermission
                                        @permission('category-delete')
                                        {!! Form::open(['method' => 'DELETE','route' => ['admin.categories.destroy', $category->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                        @endpermission

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                {!! $data->render() !!}
            </div>
        </div>
    </div>
@endsection
