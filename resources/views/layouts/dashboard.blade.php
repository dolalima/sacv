<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    {!! Html::style('components/bootstrap/dist/css/bootstrap.min.css')!!}

    <!-- MetisMenu CSS -->
    {!! Html::style('components/metisMenu/dist/metisMenu.min.css')!!}

    <!-- Custom CSS -->
    {!! Html::style('css/sb-admin-2.css')!!}

    <!-- Custom Fonts -->
    {!! Html::style('components/font-awesome/css/font-awesome.min.css')!!}

    <!-- Text Editor CSS -->
    {!! Html::style('components/froala-wysiwyg-editor/css/froala_editor.min.css') !!}

    <!-- Date Picker CSS  -->
    {!! Html::style('components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')!!}

    <!-- Custom CSS -->
    {!! Html::style('css/app.css')!!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html"><i class="fa fa-cube"></i> SACV</a>
        </div>
        <!-- /.navbar-header -->

        @include('layouts.top')
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">

            @include('layouts.nav')

        </div>
        <!-- /.navbar-static-side -->
    </nav>


    @yield('content')


</div>
<!-- /#wrapper -->

<!-- jQuery -->
{!! Html::script('components/jquery/dist/jquery.min.js')!!}

<!-- Bootstrap Core JavaScript -->
{!! Html::script('components/bootstrap/dist/js/bootstrap.min.js')!!}

<!-- Metis Menu Plugin JavaScript -->
{!! Html::script('components/metisMenu/dist/metisMenu.min.js')!!}

<!-- Text Editor JavaScript -->
{!! Html::script('components/froala-wysiwyg-editor/js/froala_editor.min.js') !!}

<!-- Date Picker JavaScript  -->
{!! Html::script('components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')!!}

<!-- Custom Theme JavaScript -->
{!! Html::script('js/sb-admin-2.js')!!}

{!! Html::script('js/custom.js') !!}

</body>

</html>
