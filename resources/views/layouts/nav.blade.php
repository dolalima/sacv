<div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
        <li class="sidebar-search">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
            </div>
            <!-- /input-group -->
        </li>
        <li>
            <a href="home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        <li>
            <a href="#"><i class="fa fa-file-text"></i> Cadastros<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                @permission('manager-list')
                <li><a href="{{ url('admin/managers') }}"><i class="fa fa-user"></i> Gestores</a></li>
                @endpermission()
                @permission('event-list')
                <li><a href="{{ url('admin/events') }}"><i class="fa fa-television"></i> @Lang('label.events')</a></li>
                @endpermission()
                @permission('category-list')
                <li><a href="{{ url('admin/categories') }}"><i class="fa fa-tags"></i> @Lang('label.categories')</a></li>
                @endpermission()
                @permission('role-list')
                <li><a href="{{ url('admin/roles') }}"><i class="fa fa-key"></i> Permissões</a></li>
                @endpermission()
            </ul>
        </li>
        <li>
            <a> <i class="fa fa-bookmark"></i> Eventos</a>
        </li>
    </ul>
</div>
