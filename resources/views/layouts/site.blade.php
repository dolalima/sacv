<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SAVC - Eventos </title>

    <!-- Bootstrap Core CSS -->
    {!!Html::style('components/bootstrap/dist/css/bootstrap.min.css')!!}

    <!-- Custom CSS -->
    {!!Html::style('css/shop-homepage.css')!!}

    <!-- Custom Fonts -->
    {!! Html::style('components/font-awesome/css/font-awesome.min.css')!!}

    <!-- Custom Style -->
    {!! Html::style('css/app.css')!!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar  navbar-collapse sacv-theme navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><i class="fa fa-graduation-cap"></i> SACV</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="events/1">Eventos</a>
                </li>
                <li>
                    <a href="#">Contato</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    @yield('content')

</div>
<!-- /.container -->

<div class="container">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
<!--                <p>Copyright &copy; Your Website 2014</p>-->
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
{!!Html::script('components/jquery/dist/jquery.js')!!}


<!-- Bootstrap Core JavaScript -->
{!!Html::script('components/bootstrap/dist/js/bootstrap.min.js')!!}

</body>

</html>
