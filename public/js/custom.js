/**
 * Created by dolalima on 17/05/17.
 */
$(document).ready(function () {
    $('.text-editor').froalaEditor({
        fontFamily: {
            "Roboto,sans-serif": 'Roboto',
            "Oswald,sans-serif": 'Oswald',
            "Montserrat,sans-serif": 'Montserrat',
            "'Open Sans Condensed',sans-serif": 'Open Sans Condensed'
        },
        fontFamilySelection: true
    });
    $('.datepicker').datepicker({
      format: 'dd/mm/yyyy',
    });
});
