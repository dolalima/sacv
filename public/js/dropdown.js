/**
 * Created by dolalima on 21/12/16.
 */
$('#states_select').change(function(event){
    var state_id = event.target.value;
    var current_city = $('#city_select');
    $.get('http://api.conecte.com.br/cities',{state_id:state_id},function(response,status){
        $('#city_select').html('');
        var selected = '';
        for(var i=0;i<response.length;i++){

            // if(response[i].id == current_city){
            //     selected = 'selected';
            // }else{
            //     selected = '';
            // }
            $('#city_select').append(
                "<option value='"+response[i].id+"' "+selected+">"+response[i].name+"</option>"
            )

        }
        $('#city_select').val('');
    },'json');

});