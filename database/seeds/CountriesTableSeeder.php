<?php

use Illuminate\Database\Seeder;
use \Illuminate\Database\Eloquent\Model;
use App\Country;


class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            ["id" => 1, "name" => "Brasil", "initials" => "BR"],
            ["id" => 2, "name" => "Estados Unidos", "initials" => "USA"]
        ];

        foreach ($countries as $country) {
            DB::insert("INSERT INTO countries (id,name,initials,created_at,updated_at) VALUES ({$country['id']},'{$country['name']}','{$country['initials']}','2016-01-01 00:00:00','2016-01-01 00:00:00');");
        }


    }
}
