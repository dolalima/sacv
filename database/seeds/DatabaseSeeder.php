<?php

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);


        Model::unguard();

        DB::insert("INSERT INTO oauth_clients (id,secret,name,created_at,updated_at) VALUES ('conecte-web','452baf5a724dd60ca4bb7ff95b08bf14','Conecte Web','2016-01-01 00:00:00','2016-01-01 00:00:00');");        



        Model::reguard();
    }
}
