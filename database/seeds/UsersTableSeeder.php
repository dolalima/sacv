
<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use App\Role;
use App\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {




        $role = Role::find(1);
        $user = User::create([
            'name' => 'Dola Lima',
            'email' => 'dolalima@gmail.com',
            'password' => bcrypt('12345'),
            'remember_token' => str_random(10),
        ]);
        $user->roles()->attach($role);
        $user->save();


    }
}
