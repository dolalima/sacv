<?php

use Illuminate\Database\Seeder;
use \Illuminate\Database\Eloquent\Model;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [["id" => 1, "name" => "ACRE", "initials" => "AC", "country_id" => 1],
            ["id" => 2, "name" => "ALAGOAS", "initials" => "AL", "country_id" => 1],
            ["id" => 3, "name" => "AMAPÁ", "initials" => "AP", "country_id" => 1],
            ["id" => 4, "name" => "AMAZONAS", "initials" => "AM", "country_id" => 1],
            ["id" => 5, "name" => "BAHIA", "initials" => "BA", "country_id" => 1],
            ["id" => 6, "name" => "CEARÁ", "initials" => "CE", "country_id" => 1],
            ["id" => 7, "name" => "DISTRITO FEDERAL", "initials" => "DF", "country_id" => 1],
            ["id" => 8, "name" => "ESPÍRITO SANTO", "initials" => "ES", "country_id" => 1],
            ["id" => 9, "name" => "GOIÁS", "initials" => "GO", "country_id" => 1],
            ["id" => 10, "name" => "MARANHÃO", "initials" => "MA", "country_id" => 1],
            ["id" => 11, "name" => "MATO GROSSO DO SUL", "initials" => "MS", "country_id" => 1],
            ["id" => 12, "name" => "MATO GROSSO", "initials" => "MT", "country_id" => 1],
            ["id" => 13, "name" => "MINAS GERAIS", "initials" => "MG", "country_id" => 1],
            ["id" => 14, "name" => "PARANÁ", "initials" => "PR", "country_id" => 1],
            ["id" => 15, "name" => "PARAÍBA", "initials" => "PB", "country_id" => 1],
            ["id" => 16, "name" => "PARÁ", "initials" => "PA", "country_id" => 1],
            ["id" => 17, "name" => "PERNAMBUCO", "initials" => "PE", "country_id" => 1],
            ["id" => 18, "name" => "PIAUÍ", "initials" => "PI", "country_id" => 1],
            ["id" => 19, "name" => "RIO DE JANEIRO", "initials" => "RJ", "country_id" => 1],
            ["id" => 20, "name" => "RIO GRANDE DO NORTE", "initials" => "RN", "country_id" => 1],
            ["id" => 21, "name" => "RIO GRANDE DO SUL", "initials" => "RS", "country_id" => 1],
            ["id" => 22, "name" => "RONDÔNIA", "initials" => "RO", "country_id" => 1],
            ["id" => 23, "name" => "RORAIMA", "initials" => "RR", "country_id" => 1],
            ["id" => 24, "name" => "SANTA CATARINA", "initials" => "SC", "country_id" => 1],
            ["id" => 25, "name" => "SERGIPE", "initials" => "SE", "country_id" => 1],
            ["id" => 26, "name" => "SÃO PAULO", "initials" => "SP", "country_id" => 1],
            ["id" => 27, "name" => "TOCANTINS", "initials" => "TO", "country_id" => 1],
            ["id" => 28, "name" => "TEXAS", "initials" => "TX", "country_id" => 2]];


        foreach ($states as $state) {
            DB::insert("INSERT INTO states (id,name,country_id,created_at,updated_at) VALUES ({$state['id']},'{$state['name']}',{$state['country_id']},'2016-01-01 00:00:00','2016-01-01 00:00:00');");
        }


    }
}
