<?php

use Illuminate\Database\Seeder;

use App\Permission;
use App\Role;

class PermissionTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {



    $permission = [
      [
        'name' => 'role-list',
        'display_name' => 'Display Role Listing',
        'description' => 'See only Listing Of Role'
      ],
      [
        'name' => 'role-create',
        'display_name' => 'Create Role',
        'description' => 'Create New Role'
      ],
      [
        'name' => 'role-edit',
        'display_name' => 'Edit Role',
        'description' => 'Edit Role'
      ],
      [
        'name' => 'role-delete',
        'display_name' => 'Delete Role',
        'description' => 'Delete Role'
      ],
      [
        'name' => 'company-list',
        'display_name' => 'Display Company Listing',
        'description' => 'See only Listing Of Company'
      ],
      [
        'name' => 'company-create',
        'display_name' => 'Create Company',
        'description' => 'Create New Company'
      ],
      [
        'name' => 'company-edit',
        'display_name' => 'Edit Company',
        'description' => 'Edit Company'
      ],
      [
        'name' => 'company-delete',
        'display_name' => 'Delete Company',
        'description' => 'Delete Company'
      ],
      [
        'name' => 'event-list',
        'display_name' => 'Display Opportunity Listing',
        'description' => 'See only Listing Of Opportunity'
      ],
      [
        'name' => 'event-create',
        'display_name' => 'Create Opportunity',
        'description' => 'Create New Opportunity'
      ],
      [
        'name' => 'event-edit',
        'display_name' => 'Edit Opportunity',
        'description' => 'Edit Opportunity'
      ],
      [
        'name' => 'event-delete',
        'display_name' => 'Delete Opportunity',
        'description' => 'Delete Opportunity'
      ],
      [
        'name' => 'manager-list',
        'display_name' => 'Display Manager Listing',
        'description' => 'See only Listing Of Manager'
      ],
      [
        'name' => 'manager-create',
        'display_name' => 'Create Manager',
        'description' => 'Create New Manager'
      ],
      [
        'name' => 'manager-edit',
        'display_name' => 'Edit Manager',
        'description' => 'Edit Manager'
      ],
      [
        'name' => 'manager-delete',
        'display_name' => 'Delete Manager',
        'description' => 'Delete Manager'
      ],
      [
        'name' => 'category-list',
        'display_name' => 'Display Category Listing',
        'description' => 'See only Listing Of Category'
      ],
      [
        'name' => 'category-create',
        'display_name' => 'Create Category',
        'description' => 'Create New Category'
      ],
      [
        'name' => 'category-edit',
        'display_name' => 'Edit Category',
        'description' => 'Edit Category'
      ],
      [
        'name' => 'category-delete',
        'display_name' => 'Delete Category',
        'description' => 'Delete Category'
      ]
      //            [
      //                'name' => '<model-name>-list',
      //                'display_name' => 'Display Item Listing',
      //                'description' => 'See only Listing Of Item'
      //            ],
      //            [
      //                'name' => '<model-name>-create',
      //                'display_name' => 'Create Item',
      //                'description' => 'Create New Item'
      //            ],
      //            [
      //                'name' => '<model-name>-edit',
      //                'display_name' => 'Edit Item',
      //                'description' => 'Edit Item'
      //            ],
      //            [
      //                'name' => '<model-name>-delete',
      //                'display_name' => 'Delete Item',
      //                'description' => 'Delete Item'
      //            ]
    ];

    foreach ($permission as $key => $value) {
      Permission::create($value);
    }
    $permissions = Permission::all();
    $role = Role::create(['name'=>'manager','display_name'=>'Manager','description'=>'full system access']);
    $role->save();
    foreach ($permissions as $permission) {
      $role->roles()->attach($permission->id);
    }
    $role->save();
  }
}
