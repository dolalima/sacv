<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('birthday')->nullable();
            $table->string('rg')->nullable();
            $table->string('email')->nullable();
            $table->string('cpf')->nullable()->unique();
            $table->string('nickname')->nullable()->unique();
            $table->string('phone')->nullable();
            $table->string('cellphone')->nullable();
            $table->integer('photo_id')->nullable()->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('address_id')->nullable()->unsigned();
            $table->string('type');
            $table->timestamps();

            // START SIM database columns
            $table->integer('program_id')->nullable();
            $table->integer('center_id')->nullable();
            $table->integer('year')->nullable();
            // END

            // FOREIGN
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('photo_id')->references('id')->on('photos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people');
    }
}
